@extends('layouts.welcome')

@section('section2')
    <h1>Ini adalah page 2</h1>
    <p>Dimana disini menggunakan extends, section dan for</p>

    <h2>Mari Berhitung 1 - 10</h2>

    @for ($i = 0; $i < 10; $i++)
        {{$i}} <br>
    @endfor
@endsection
